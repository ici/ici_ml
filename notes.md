# Machine Learning pour apprendre les caractéristiques des IRIS les plus à risques d’être le lieu de contaminations

**<u>Objet :</u>**

Grâce à la [modélisation géographique réalisée pour le projet ICI](https://ici.saclay.inria.fr/generic_dataBuilding.html), entraîner un algorithme de machine learning pour prédire certaines caractéristiques des vagues épidémiques. Les vagues étant assez différentes, selon les variants et la réponse des autorités sanitaires, ce serait dans un premier temps plus simple de comparer les villes entre elles à vague fixe (la modélisation géographique peut désormais être appliquée en France entière). Avec des données de présence adaptées aux restrictions sanitaires, il serait possible de prédire où et comment se manifestent les vagues, à variant constant. Ce travail aurai donc une visée prospective, pour prédire où et comment l’épidémie se développera dans une ville, et explicative, afin de déterminer quels sont les facteurs structurels qui font que certaines zones sont plus touchés par l’épidémie. 

## Que veut on apprendre ? 
Plusieurs questions, dans l’ordre de pertinence :

  * Où est ce que les vagues débutent. 
  * Durée à une valeur de taux d’incidence supérieure à un certain seuil critique. 
  * La forme des vagues (jusqu'où monte la phase ascendante, combien de temps dure le plateau, quelle est la vitesse de la décroissance).

## Données d’entrée 
### Échelle : 
L’approche microscopique déployée dans le projet ICI nous amène à utiliser l’échelle des [IRIS](https://fr.wikipedia.org/wiki/Îlots_regroupés_pour_l%27information_statistique). Cet agrégat est le plus fin possible pour la diffusion de statistiques et des données du taux d’incidence sont disponibles à cette échelle, France entière.  
Peut être que ça marcherait mieux sur les quartiers ou les arrondissements, mais les données épidémio ne sont pas plus précises à cette échelle. Il sera toutefois possible d’agréger. 

### Entrée :
Les des statistiques agrégées concernant la structure urbaine, la population synthétique et la présence d’individus, avec une quarantaine de variables pour chacune deux premières familles.
Certaines de ces valeurs peuvent être consultés ici : <https://ici.saclay.inria.fr/data-IRIS-Paris.html> (pas à jour et il doit manquer certaines valeurs – mieux vaut consulter le .gpkg du projet). 

Les valeurs de présences pourraient être soit une désagrégation du mobiliscope, soit une désagrégation des différents flux modélisés pour créer les données d’ICI, soit une agrégation de la présence simulée par le module épidémio de ICI. 
Ils ne reflètent pour l’instant pas les différentes politiques sanitaires, mais il serait possible d’avoir des flux adaptés pour la première et la deuxième/troisième vagues (celles ci étant franchement confondues). Peu de mesures ont ensuite été prises lors des vagues suivantes alors on pourrait dire qu’elle suivent un régime normal. 
Certaines parties du générateur de données ICI étant historicisé, il serait possible de générer une modélisation de la ville correspondant à la date voulue. 

### Sorties : 
Pour chaque IRIS, on a un nombre de tests réalisés et un nombre de tests positifs. On peut donc en déduire un taux d'incidence, un taux de dépistage et un taux de positivité. Par contre, il y a des IRIS ou une faible population est recensée mais ou de nombreux cas ont été déclarés (personnes à la rue ?). De nombreux taux sont donc très imporntants et il serait peut être mieux de travailler avec le nombre de tests positifs plutôt que le taux d'incidence. 
À savoir que l’utilisation de cette mesure pose problème parmi les épidémios, car elle est très dépendante du nombre de tests réalisés. Il est convenu qu’il vaut mieux utiliser les taux d’hospitalisation combinés avec les tables de sévérités afin d’avoir le nombre de cas positifs. Néanmoins les données hospitalières sont publiées à l’échelle du département. 
À mon avis, l’originalité du projet reposant sur la précision des traitements, on devrait utiliser ces derniers. 

La localisation est déclarative, mais représente le lieu de résidence des individus, alors c’est peut être moins les activités d’une zone qui seront importantes et plus la population (avec caractéristiques socio économiques) et les déplacements. 


Comment synthétiser les données ? Est il possible de faire apprendre sur une distribution temporelle plutôt que sur un scalaire ?
Pour l’instant dans les données : 

   * Somme de tous les cas positifs
   * Somme de tous les cas positifs pour chaque vagues (délimitée à la main) 

En projet : 

   * Pour un nombre d’IRIS donné, l’ordre dans lequel les IRIS voient leur taux d’incidence augmenter . 
   * Durée au taux au dessus du seuil critique 


Demander des données plus précises à l’INSERM ? Dans le cadre du projet ICI, nous sommes en relation avec l’équipe CRESS, il disposent certainement de données épidémiques plus précises, qu’elles soient mesurées ou synthétiques. 
Test sur un autre cas d’étude : 
Une fois que le modèle aura appris sur Paris, l’appliquer sur Lyon pour validation. C’est une ville qui ressemble à Paris (au contraire de Périgueux). 
 

## Moran 

Moran fait pour mesurer l'autocorrélation spatiale de la ***sumPositiveCases***. 
Concerne donc toutes les vagues.

<img title="spatial correlation of sumPositiveCases in Paris IRIS" alt="spatial correlation of FatMean in Paris IRIS" src="./img/moranQuadrant.png">

<img title="spatial correlation of sumPositiveCases in Paris IRIS" alt="spatial correlation of FatMean in Paris IRIS" src="./img/moranPlot.png">

<code>
	Moran I test under randomisation

data:  iris$epid_sumPositiveCases  
weights: nbNeighWeighted    

Moran I statistic standard deviate = 14.98, p-value < 2.2e-16
alternative hypothesis: greater

sample estimates:

Moran I statistic       Expectation          Variance 

     0.2975115523     -0.0010090817      0.0003971233 
</code>


## Premier essai : Random Forest
*fichier randomForest.R*

Dans un premier temps, travail sur toutes les valeurs de l'épidémie avec l'apprentissage sur la valeur ***sumPositiveCases***.
Apprentissage sur toutes les propriétés du modèle urbain. 
Mieux de faire un set d'apprentissage et un set de validation que de se reposer sur le bagging interne à l'algorithme.
Set d'apprentissage de 75% des IRIS.

Par contre, donne des résultats très variables (réplications 25 fois)
Pour l'instant sélection aléatoire, nécessaire d'utiliser les k foldings pour un meilleur sampling (Alex)
Possible de faire le test sur un seul agrégat, l'apprentissage sur le reste, et d'itérer pour que tous les agrégats soient testés. (fait par Maxime)


Calcul du RMSE pour estimer la difference entre la prédiction sur les données de validations et une attribution aléatoire. 


@todo Faire un ACP pour réduire la dimention des données d'entrées. Ça peut améliorer de quelques % la capacité d'apprentissage et ce sera un bon apprentissage sur les données d'entrée


Faire un ACP fonctionnel pour que les données temporelles soient réduites de 500 jours à quelques fonctions de réference. 
Faire ensuite apprendre sur ces fonctions.

Voir aussi la méthode de univariate regression analysis method ARIMA ? 


### Discrétiser par vague et par arrondissement 

@todo En gros, trois vagues à discretiser. Nécessaire de les faire tenir sur des longueurs similaires (compléter par des valeurs nulles si trop long)

Est il possible de deviner où sont les vacances scolaires dans le jeu d'apprentissage ? 

## Pistes de développements, idées 
projection des données de sortie dans un espace fonctionnel (ACP) (Yann).

Améliorations : 

Ce intéressant de prendre en compte la proximité géographique entre les IRIS? Afin d'interpoler une courbe d'incidence ? 

## Biblio 
Les deux domaines étant assez riches en intérêt et en publis, pas mal de papiers ont travaillé sur le sujet. Je n’ai pas tout lu du tout, mais ça ne semble jamais travailler à l’échelle micro à laquelle nous nous intéressons. 

Application of machine learning in the prediction of COVID-19 daily new cases: A scoping review
<https://www.sciencedirect.com/science/article/pii/S2405844021022465>

Omicron Case Prediction using Machine Learning
<https://ieeexplore.ieee.org/abstract/document/10079756>

Forecast and prediction of COVID-19 using machine learning
<https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8138040/>
COVID-19 Cases in India: Prediction and Analysis using Machine Learning
<https://link.springer.com/chapter/10.1007/978-981-19-0707-4_50>
Ces deux papiers utilisent un modèle ARIMA pour prédire des séries temporelles. 

AI-Driven Tools for Coronavirus Outbreak: Need of Active Learning and Cross-Population Train/Test Models on Multitudinal/Multimodal Data
<https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7087612/>

Exploring Socioeconomic Status as a Global Determinant of COVID-19 Prevalence, Using Exploratory Data Analytic and Supervised Machine Learning Techniques: Algorithm Development and Validation Study
<https://formative.jmir.org/2022/9/e35114/>

A Benchmark Study by using various Machine Learning Models for Predicting Covid-19 trends
<https://arxiv.org/abs/2301.11257>

COVID-19 Prediction Using Time Series Models
<https://ieeexplore.ieee.org/abstract/document/10074595>


Étude de déterminismes sociaux à l'échelle de l'IRIS 
<https://www.santepubliquefrance.fr/revues/articles-du-mois/2022/l-impact-de-la-defavorisation-sociale-sur-la-dynamique-d-infection-au-sars-cov-2-en-france-entre-mai-2020-et-avril-2021>
